; ati demo 2020
; polich 22 apr 2020

pro ati0

; load complex interferogram
t0=complexarr(15000,12000)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\interferogram_orkney.bin'
readu,1,t0
close,1

; separate into real and imaginary part
re0=real_part(t0)
im0=imaginary(t0)
t0=0
; the phase of the interferogram corresponds with the phase shift ~3s

; compute intensity and phase at full resolution
pw0=sqrt(re0^2+im0^2)
ph0=atan(im0,re0) ; shows signatures, but noisy

; coherence - phases consistent in neighborhood of pixels
; average over 15x13 pixels to reduce to 1000x1000 array
re1=rebin(re0,1000,1000) ; real_part
im1=rebin(im0,1000,1000) ; imaginary part
pw1=rebin(pw0,1000,1000)
ph1=atan(im1,re1)
  ; red areas correspond with phases "wrapping around"
  ; to correct, must subtract 2*pi
co1=sqrt(re1^2+im1^2)/pw1 ; coherence estimate, results in a shorter vector
  ; mean = 0.78883219
  ; can also think of the phases as velocities
  ; good coherence except in very dark areas, at bottom of image

; convert to horizontal velocities
th=31.0*!pi/180.0
tau=25.0/7596.0 ; m/s or roughly 5 miles/s
f=9.65e+9
c=2.9979e+8
vfac=c/(4.0*!pi*tau*f*sin(th))
uh1=vfac*ph1
; velocity range:
; print,min(uh1),max(uh1) = -4.58167, 4.58114


; further smoothing
; a better way would start with a mask over land
re2=re1
im2=im1
for k=0,19 do begin
    re2=smooth(re2,3,/edge_truncate)
    im2=smooth(im2,3,/edge_truncate)
endfor
ph2=atan(im2,re2)
uh2=vfac*ph2

; Compare:
; loadct,33
; tvscl,uh1>(-3.0)<3.0
; tvscl,uh2>(-3.0)<3.0

window,0,xsize=1000,ysize=1000

stop

end