; oce 687 midterm exam, 2020
; extract internal wave signatures from an experiment in the Gulf of Mexico
; polich 10 mar 2020

pro iwmexmid0

  t0=fltarr(1500,3000) ; will contain a TerraSAR-X interferogram amplitude image
  ; (note t0 proportional to σ0, but not calibrated
 
  ; read in simple floating-point array of 1500×3000 pixels from a simple binary file
  ; (read in the file on home system)
  openr,1,'C:\Users\Admin\Documents\IDLWorkspace\front.flt'
  ; (read in the file on portable system)
  ;openr,1,'C:\Users\evo\Documents\IDLWorkspace\front.flt'
  ; (read in the file on instructor system)
  ;openr,1,'C:\roland\oce687\2020\midterm\front.flt'  
  readu,1,t0
  close,1

  ; steps to facilitate reviewing each step of the assignment separately
  goto,step1
  ;goto,step2
  ;goto,step3

step1:

  ; display the original image, including a pronounced bright front in the ocean
  ;window,0,xsize=1500,ysize=3000
  ;loadct,0
  ;tvscl,t0<10000.0  ; recommended linear intensity threshold [because 'print,mean(t0)' = 10269.1]

  ; hard code first crop variables
  croph1=910
  croph2=1189
  cropha=280
  cropv1=460
  cropv2=1397
  cropva=938

  ; crop image size to subset containing only the location of the front (avoid outlier pixels with a close crop)
  t1=t0(croph1:croph2,cropv1:cropv2)
  
  ; display the cropped image subset alone
  ;window,1,xsize=cropha,ysize=cropva,xpos=1000,ypos=100
  ;loadct,0
  ;tvscl,t1<10000.0  ; recommended linear intensity threshold

  sb0=fltarr(cropha,cropva)   ; subset 280x938 array elements (set to zero)
  s0=smooth(1.0*t1,4,/edge_truncate)  ; smooth the initial results to obtain a smooth line
  ;s0(where(s0 gt 4870.0))=100000.0  ; highlight only pixels over linear intensity threshold 4870.0 as 100000.0
 
  for j=0,cropva-1 do begin
    sb0(i,j)=max(s0(*,j),i) ; find the brightest pixel in each line
  endfor
  
  ; [assignment note] it was not clearly specified whether "version of the image" is the original image or a subset "in several 100 lines"
  ; display a version of the subset image only on the screen in which the shape of the front is highlighted as a smooth white line
  ;window,2,xsize=cropha,ysize=cropva
  ;loadct,0
  ;tvscl,sb0<10000.0
 
  ; display a version of the original image on the screen in which the shape of the front is highlighted as a smooth white line (alone)
  t2=fltarr(1500,3000)
  t2(croph1:croph2,cropv1:cropv2)=sb0
  window,2,xsize=1500,ysize=3000
  loadct,0
  tvscl,t2<10000.0

; TASK: try instead to median-filter the initial results to obtain a smooth line (not done)


; [assignment note] it was unclear to me at first how to proceed; this method (which I like better) was implemented first, before the line-by-line max method
; [assignment note] this method below produces a thick clearly visible, smooth line, but may not meet the requirments (for a single pixel line) -- left commented out
; smooth the initial results to obtain a smooth line
;s0=smooth(1.0*t1,16,/edge_truncate)
;s0(where(s0 gt 4870.0))=100000.0  ; highlight only pixels over threshold 4870.0 as 100000.0
;
;s1=fltarr(cropha,cropva)   ; 280x938 array elements
;s1=t1
;s1(where(s0 eq 100000.0))=100000.0  ; highlight only pixels over threshold 10000.0
;
; display a version of the subset image only on the screen in which the shape of the front is highlighted as a smooth thick white line
;window,3,xsize=cropha,ysize=cropva,xpos=1050,ypos=150
;loadct,0
;tvscl,s1<10000.0

; [assignment note] it was not clearly specified whether "version of the image" should include the original image as a background or not
; display a version of the original image on the screen in which the shape of the front is highlighted as a smooth thick white line (with original backdrop)
;t2=t0
;t2(croph1:croph2,cropv1:cropv2)=s1
;window,3,xsize=1500,ysize=3000
;loadct,0
;tvscl,t2<10000.0


;stop
;step2:


; further crop image size to subset containing only the location of the front along with all surrounding waters to compute relative intensity
crop_bottom=460
crop_top=1327    ; also crop out land feature
crop_height=867
t3=t0(*,crop_bottom:crop_top)

; display the cropped image subset alone, excluding land feature
;window,1,xsize=1500,ysize=(crop_top-crop_bottom),xpos=100,ypos=-900
;loadct,0
;tvscl,t3<10000.0

  ; compute the mean intensity profile of the front
  ; 1500 pixels horizontal, 867 pixels vertical
  ; average in 2nd dimension -- vertical will have 1500 pixels with average value
  ; uses a technique similar to the one we developed to extract the internal wave signature profile in class
  p1=mean(t3,dimension=2)
  ; show the mean intensity profile of the front as a plot in another window on the screen
  window,3,xsize=500,ysize=700
  loadct,0
  plot,t3(*,crop_height),thick=0.1,title="Mean Intensity"    ; vertical range of plot
  oplot,p1,thick=4.0
  ; print the mean intensity profile of the front
  print,'mean (p1) ',mean(p1)
  print,'max (p1) ',max(p1)
  print,'max(p1)/mean(p1) ',max(p1)/mean(p1)


;stop
;step3:


  ; try to find out how much brighter this front is compared to the surrounding waters
  ; compute the relative intensity of the front
  ; (obtain approximately the same peak brightness relative to the ambient brightness obtained by Dr. Romeiser from his calculation)
  ; improved approach with individual shifting before averaging
  imax=intarr(crop_height)
  for j=0,crop_height-1 do begin
    pj=t3(*,j)
    for k=0,4 do pj=smooth(pj,5,/edge_truncate)   ; to reach goal "a little more than 3.0" -- the smoothing width and iterations are 'tweaked'
    imax(j)=(where(pj eq max(pj)))(0)  ;+crop_height
  endfor
  
  intense_rad=min([min(imax),1499-max(imax)]) ; radius for intensity comparison

  p2=fltarr(2*intense_rad+1)  ; contains twice the intense_rad length, plus 1

  ; new mean profile with better intensity profile (slightly more intense)
  for j=0,crop_height-1 do begin
    p2=p2+t3(imax(j)-intense_rad:imax(j)+intense_rad,j)*0.005
  endfor

  ; print the relative intensity profile of the internal wave front
  print,'radius of relative intensity ',intense_rad
  print,'mean (p2) ',mean(p2)
  print,'max (p2) ',max(p2)
  ; print peak brightness relative to the ambient brightness
  print,'max(p2)/mean(p2) ',max(p2)/mean(p2),'  <<< relative intensity of internal wave to ocean background'

  ; show the relative intensity profile of the front as a plot in another window on the screen
  window,4,xsize=500,ysize=600
  loadct,0
  plot,p2,thick=4.0,title="Relative Intensity"

  stop

end