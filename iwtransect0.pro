; extracting internal wave signatures
; polich 11 feb 2020

pro iwtransect0

t0=fltarr(1500,5000)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\dongsha_pw.flt'
readu,1,t0
close,1

window,0,xsize=1000,ysize=1000
loadct,0

t1=t0(0:999,200:1199)
t1=rot(t1,-29.0)

t2=10.0*alog10(t1)
t2(*,400)=max(t2)
t2(*,600)=max(t2)
tvscl,t2

window,1,xsize=1000,ysize=400

; plot individual intensity profiles
; test if rotation to view internal waves is highlighted in color
;plot,[0,1000],[0,60000],psym=3
;loadct,33
;for j=0,199 do oplot,t1(*,400+j),color=j+25

; * = horizontal, 200 pixels vertical, average in 2nd dimension
; vertical will have 1000 pixels with average value
p1=mean(t1(*,400:599),dimension=2)
;loadct,0
;plot,t1(*,500)
;oplot,p1,thick=3.0

stop


; improved approach with individual shifting before averaging
imax=intarr(200)
for j=0,199 do begin
    pj=t1(200:599,400+j)
    for k=0,4 do pj=smooth(pj,11,/edge_truncate)
    imax(j)=(where(pj eq max(pj)))(0)+200
endfor

; new mean profile with better intensity profile (slightly more intense)
p2=fltarr(401)
for j=0,199 do p2=p2+t1(imax(j)-200:imax(j)+200,j)*0.005

plot,p1
oplot,p2


; rebin image should have 20% speckle noise of congrid image
;tvscl,t0(0:999,0:999)
;tvscl,10.0*alog10(congrid(t0,300,1000))
;tvscl,10.0*alog10(rebin(t0,300,1000))


stop

end