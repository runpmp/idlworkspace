; simulation of an antenna beam pattern
; based on antenna0 23 jan 2020
; roro 24..27 jan 2020

; - automatic step by step animation
; - power pattern and 1/e power width calculation at the end

pro antenna1

la =   1.0    ; antenna length   [m]
lam=   0.03   ; radar wavelength [m]
r0 =1000.0    ; nominal distance [m]

na = 129      ; number of antenna elements

ng =2001      ; number of locations on ground
dxg =  0.1    ; ground step    [m]

; antenna element locations
xa=la*(indgen(na,/float)/(na-1)-0.5)
xac=bytarr(na)   ; marks used elements

; ground locations
xg=dxg*(indgen(ng)-ng/2)

; signal phase on ground
phg=fltarr(ng)

; sum amplitude
res=fltarr(ng)
ims=fltarr(ng)

window,0,xsize=900,ysize=400,/pixmap
window,1,xsize=800,ysize=200,xpos=1100,ypos=0
window,2,xsize=800,ysize=300,/pixmap
window,3,xsize=800,ysize=300,xpos=1100,ypos=250
window,4,xsize=800,ysize=400,xpos=1100,ypos=600
dd2r=bytarr(800,300)
dd2g=bytarr(800,300)
dd3r=bytarr(800,300)
dd3g=bytarr(800,300)
dd3b=bytarr(800,300)

; step size loop
for is=0,round(alog2(na-1)) do begin
    if is eq 0 then begin
       dia=0
       ns=0
    endif else begin
       dia=(na-1)/(2^is)
       ns=(na-1)/dia
    endelse

    ; antenna element loop
    for ia0=0,ns do begin
        if is eq 0 then ia1=(na-1)/2 else ia1=ia0*dia

        if xac(ia1) eq 0 then begin

           ; show antenna elements used so far
           wset,0
           loadct,0,/silent
           plot,[-1.0,1.0],[0.0,1.0],position=[0.0,0.0,1.0,1.0],psym=3
           oplot,[-0.5,0.5],[0.5,0.5]
           for ia=0,na-1 do if xac(ia) eq 1 then oplot,[xa(ia),xa(ia)],[0.5,0.5],psym=7,symsize=2.0,thick=1.0
           loadct,33,/silent
           oplot,[xa(ia1),xa(ia1)],[0.5,0.5],psym=7,color=224,symsize=2.0,thick=2.0
           dd=tvrd(true=1)
           dd=dd(*,50:849,100:299)
           wset,1
           tv,dd,true=1

           ; range and phase on the ground for current antenna element
           rg=sqrt((xg-xa(ia1))^2+r0^2)
           phg=2.0*!pi*rg/lam
           if ia0 eq 0 then ph0=phg((ng-1)/2)
           phg=phg-ph0

           ; add phase plot to phase diagram
           wset,2
           loadct,0,/silent
           plot,[xg(0),xg(ng-1)],[-4,4],psym=3,position=[0.08,0.17,0.98,0.98], $
                xtitle='Distance from Footprint Center [m]',                   $
                ytitle='Relative Phase [rad]',                                 $
                xcharsize=1.5,xticks=10,xminor=4,xstyle=1,                     $
                ycharsize=1.5,yticks= 8,yminor=2,ystyle=1
           loadct,33,/silent
           oplot,xg,atan(sin(phg),cos(phg)),color=224,thick=1.0
           dd2=tvrd(true=1)
           wset,3
           dd3=tvrd(true=1)
           dd3r(*,*)=dd3(0,*,*)
           dd3g(*,*)=dd3(1,*,*)
           dd3b(*,*)=dd3(2,*,*)
           ; set all lines in previous version to white
           w=where(dd3r gt 0,c)
           if c eq 0 then dd3=dd2 else begin
              dd3r(w)=255
              dd3g(w)=255
              dd3b(w)=255
              ; copy new red line into old diagram
              dd2r(*,*)=dd2(0,*,*)
              dd2g(*,*)=dd2(1,*,*)
              w=where(dd2r ne dd2g)
              dd3r(w)=255
              dd3g(w)=  0
              dd3b(w)=  0
              dd3(0,*,*)=dd3r(*,*)
              dd3(1,*,*)=dd3g(*,*)
              dd3(2,*,*)=dd3b(*,*)
           endelse
           tv,dd3,true=1

           ; add complex amplitude to sum amplitude and display
           res=res+cos(phg)
           ims=ims+sin(phg)
           ams=sqrt(res^2+ims^2)
           ams=ams/max(ams)
           wset,4
           loadct,0,/silent
           plot,xg,ams,position=[0.08,0.12,0.98,0.98],thick=1.0, $
                xtitle='Distance from Footprint Center [m]',     $
                ytitle='Relative Amplitude',                     $
                xcharsize=1.5,xticks=10,xminor=4,xstyle=1,       $
                ycharsize=1.5
           loadct,33,/silent
           oplot,xg,ams,color=160,thick=2.0

           xac(ia1)=1
           wait,0.1

        endif
    endfor
    wait,1.0

endfor

stop

; now display power distribution
pws=res^2+ims^2
pws=pws/max(pws)
loadct,0,/silent
plot,xg,pws,position=[0.08,0.12,0.98,0.98],thick=1.0, $
     xtitle='Distance from Footprint Center [m]',     $
     ytitle='Relative Power',                         $
     xcharsize=1.5,xticks=10,xminor=4,xstyle=1,       $
     ycharsize=1.5

; 1/e power width
w=where(pws gt 1.0/!const.euler,c)
print,'power 1/e width =',dxg*(c-1),' m'
loadct,33,/silent
oplot,xg(w),pws(w),color=128,thick=2.0
oplot,[xg(min(w)),xg(min(w))],[0,1],color=128,thick=2.0
oplot,[xg(max(w)),xg(max(w))],[0,1],color=128,thick=2.0

; 1/2 power width
w=where(pws gt 0.5,c)
print,'power 1/2 width =',dxg*(c-1),' m'
loadct,33,/silent
oplot,xg(w),pws(w),color=64,thick=1.0
oplot,[xg(min(w)),xg(min(w))],[0,1],color=64,thick=2.0
oplot,[xg(max(w)),xg(max(w))],[0,1],color=64,thick=2.0

stop

end