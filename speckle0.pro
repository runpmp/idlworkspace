; speckle noise reduction test
; polich 13 feb 2020
; must be careful with speckle filters -- not to change intensity or introduce artifacts
; for example speckle filter (@ 11 pixels) before a texture analysis (@ 5 pixels) = wrong
; the texture analysis must occur at lower resolution than the speckle filter is applied

pro speckle0

;t0=read_tiff('C:\Users\Admin\Documents\IDLWorkspace\ice.tif')
t0=read_tiff('C:\Users\evo\Documents\IDLWorkspace\ice.tif')
t0=reverse(t0,2)

t0=t0(0:999,0:999)
sx=(size(t0))(1)
sy=(size(t0))(2)

window,0,xsize=1000,ysize=1000
tvscl,10.0*alog10(t0)>(-25.0)<0.0

; filter window size
fs=11

; filter 1: simple boxcar smoothing
t1=smooth(t0,fs)

; filter 2: median
; good for speckle noise
t2=median(t0,fs)

; filter 3: simple adaptive filter
; averages only within +/- one standard deviation around local mean
t3=t0
c3=intarr(sx,sy)
for i=fs/2,sx-1-fs/2 do for j=fs/2,sy-1-fs/2 do begin
    ; local subimage of filter window size
    tf=t0(i-fs/2:i+fs/2,j-fs/2:j+fs/2)
    ; mean value and mean square value within frame
    mtf1=mean(tf)
    mtf2=mean(tf^2)
    ; standard deviation within frame
    std=sqrt(abs(mtf2-mtf1^2))
    ; average only values within 1 std around mean
    w=where(tf gt mtf1-std and tf lt mtf1+std,c)
    t3(i,j)=mean(tf(w))
    c3(i,j)=c
endfor

; filter 4: advanced adaptive filter
; replaces only the pixel values that are outside the std dev range
; still averages within +/- one standard deviation around local mean
; introduces some ring affects due to affecting surrounding pixels
t4=t0
c4=intarr(sx,sy)
for i=fs/2,sx-1-fs/2 do for j=fs/2,sy-1-fs/2 do begin
  ; local subimage of filter window size
  tf=t0(i-fs/2:i+fs/2,j-fs/2:j+fs/2)
  ; mean value and mean square value within frame
  mtf1=mean(tf)
  mtf2=mean(tf^2)
  ; standard deviation within frame
  std=sqrt(abs(mtf2-mtf1^2))
  ; act only if center pixel is outside the 1 std dev range
  if abs(t0(i,j)-mtf1) gt std then begin
    ; average only values within 1 std around mean
    w=where(tf gt mtf1-std and tf lt mtf1+std,c)
    t4(i,j)=mean(tf(w))
    c4(i,j)=c
  endif else c4(i,j)=1
endfor

; filter 5: more advanced adaptive filter
; used asymmetric lower and upper limits to account for
; exponential distribution and to conserve mean intensity better
t5=t0
c5=intarr(sx,sy)
for i=fs/2,sx-1-fs/2 do for j=fs/2,sy-1-fs/2 do begin
  ; local subimage of filter window size
  tf=t0(i-fs/2:i+fs/2,j-fs/2:j+fs/2)
  ; mean value and mean square value within frame
  mtf1=mean(tf)
  mtf2=mean(tf^2)
  ; standard deviation within frame
  std=sqrt(abs(mtf2-mtf1^2))
  tmin=mtf1-2.0/3.0*std
  tmax=mtf1+4.0/3.0*std
  ; act only if center pixel is outside the 1 std dev range
  if t0(i,j) lt tmin or t0(i,j) gt tmax then begin
    ; average only values within 1 std around mean
    w=where(tf gt tmin and tf lt tmax,c)
    t5(i,j)=mean(tf(w))
    c5(i,j)=c
  endif else c5(i,j)=1
endfor

; filter 6: iterative replacement of local extreme values
t6=t0
for k=0,199 do begin
  t60=t6
  c=0L
  for i=fs/2,sx-1-fs/2 do for j=fs/2,sy-1-fs/2 do begin
    ; local subimage of filter window size
    tf=t60(i-fs/2:i+fs/2,j-fs/2:j+fs/2)
    if t60(i,j) eq min(tf) or t60(i,j) eq max(tf) then begin
       t6(i,j)=0.5*(t60(i,j)+median(tf))
       c=c+1
    endif
  endfor
  tvscl,10.0*alog10(t6)>(-25.0)<0.0
  print,k,c
endfor

; if applying multiple filters in sequence it's important to have a
; smaller window for each subsequent filter
; or eg. texture analysis should occur on larger window than speckle
; filter or over the original unfiltered image
; also follows for wave retrieval

; IDL> plot,t0,t5,psym=3
; IDL> oplot,t0,t5,psym=3
; IDL> plot,10.0*alog10(t0),10.0*alog10(t5),psym=3

stop

end
