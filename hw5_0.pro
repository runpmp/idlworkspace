; oce 687 homework 5, 2020
; polich 26 apr 2020

pro hw5_0

  ; Contains a simulated smoothed SAR image of 400×400 pixels.
  f0=fltarr(400,400)
  ; (read in the file on home system)
  openr,1,'C:\Users\Admin\Documents\IDLWorkspace\hw5.flt'
  ; (read in the file on instructor system)
  ;openr,1,'C:\roland\oce687\2020\hw5\hw5.flt'
  readu,1,f0
  close,1

  ; Display the original image for comparison
  window,0,xsize=400,ysize=400,title="SAR image NRCS"
  loadct,0
  tvscl,f0

  ; Let's pretend we want to convert this NRCS array into a retrieved wind field and
  ; we don't need to worry about the wind direction. As usual, the Geophysical Model Function (GMF)
  ; for the relationship between NRCS and wind speed is given as function of the wind speed.
  ;
  ; The following values are known:
  ; Wind Speed [m/s]  NRCS
  ;     3.0           0.10
  ;     4.0           0.29
  ;     5.0           0.44
  ;     6.0           0.55
  ;     7.0           0.64
  ;     8.0           0.71
  ;     9.0           0.76
  ;     10.0          0.80
  ; Values between these given data points can be obtained by interpolation.

  ; create two arrays to map these values to x and y
  y = [0.10, 0.29, 0.44, 0.55, 0.64, 0.71, 0.76, 0.80]
  x = [3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]

  ; generate possible Wind Speed values for gmfx evenly spaced in the GMF, between min(gmfx) = 3.0 and max(gmfx) = 10.0 inclusive
  gmfx = findgen(400,increment=(280.0/399.0),start=120.0)/40


  ; The GMF for the relationship between the NRCS and wind speed should be representable as a smooth curve
  ; on a plot (similar to "Sea Surface Processes" Slide 12) and will therefore be interpolated using a spline.

  ; From the IDL manual: Splines are interpolating functions that are specifically designed to be smooth.
  ; Setting the SPLINE keyword tells INTERPOL to use cubic splines, which ensures that the interpolating
  ; function and its first and second derivatives are continuous everywhere including the tabulated points.

  ; Implement the given GMF in such a way that it can be used for wind retrievals:
  gmfy = interpol(y, x, gmfx, /spline)
  ; [Note: for the interpolated gmfy, max(gmfy) = 0.80000001, which is slightly wrong by the GMF, although this won't affect our f0]

  ; Convert the image f0 into a wind field w0 (with the same resolution)
  w0 = fltarr(400,400)
  ; For each row in f0, lookup each value in gmfy, using the value_locate function, which returns an array of indices.
  ; If an element of f0 lies between two elements of gmfy, value_Locate rounds down to the lower index.
  ; Then using the array of indices as a map for w0, assign the value of each indice from gmfx = the Wind Speed.
  for nextr=0,399 do begin
    w0(*,nextr) = gmfx[ value_locate(gmfy,f0(*,nextr)) ]
  endfor

  ; Display a smooth wind field with correct values in color in another window.
  window,1,xsize=400,ysize=400,title="Wind Field"
  loadct,34
  tvscl,w0
  ; Ensure the wind field does not exhibit computational artifacts.
  ; [assignment note] There appear to be no artifacts: the chosen color scheme influences the smoothness of the field.

  ; The pixel values can be assumed to represent good estimates of the NRCS.
  print,"Min:",min(f0),"  ->",min(w0)," m/s"
  print,"Mean:",mean(f0),"  ->",mean(w0)," m/s"
  print,"Max:",max(f0),"  ->",max(w0)," m/s"
  ; The corresponding Wind Speed values are consistent with the curve produced by the GMF

  ; Try to keep the code simple and short: Ensure the code has a reasonable computation time (not more than a few seconds).
  ; [assignment note] Yes, the chosen method seems to work correctly and quickly. But I'm not sure if the chosen curve is best.
  ; I think that the interpolation curve could be modified or experimented with. For example, another function that I would
  ; try out is the spline function, instead of interpol using the 'spline' switch.

  ; Check the plot of a random slice
  ;window,2
  ;plot,f0(22,*)
  ;window,3
  ;plot,w0(22,*)
  ;window,4
  ;plot,w0(22,*)/f0(22,*)/10 ; shows the GMF relationship here

  stop

end