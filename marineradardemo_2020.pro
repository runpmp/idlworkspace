; simulates time series of marine radar images for fourier analysis
; based on marineradardemo0 15 apr 2019
; roro 26..27 apr 2020

pro marineradardemo_2020

nx0=512   ; initial array size
nx1=256   ; size of center part of images that will be used, to avoid periodic boundary conditions
dx0=2.0   ; spatial resolution [m]
nt0=256   ; number of time steps
dt0=2.0   ; time step [s]; try 1.0, 2.0, 3.0 to see aliasing effects
  ; moving by 3/4 of wavelength foreward is same as 1/4 wavelength backward

; wavenumber array
dk0=2.0*!pi/(dx0*nx0)
kx0=fltarr(nx0,nx0)
for ikx=0,nx0-1 do kx0(ikx,*)=(ikx-nx0/2)*dk0
ky0=fltarr(nx0,nx0)
for iky=0,nx0-1 do ky0(*,iky)=(iky-nx0/2)*dk0
ka0=sqrt(kx0^2+ky0^2)

; wave frequency array according to dispersion relation
om0=sqrt(9.81*ka0)

; generate nice looking input power spectrum
s0=fltarr(nx0,nx0)
for ikx=0,nx0-1 do for iky=0,nx0-1 do if ka0(ikx,iky) ge dk0*8 then begin
    arg=-(atan(iky-nx0/2,ikx-nx0/2)/0.3)^2
    if arg gt -10.0 then begin
       s0(ikx,iky)=1.0e-6*(ka0(ikx,iky))^(-2)*exp(arg)
    endif
endif
s0=s0-5.0e-6
for is=0,9 do begin
    s0=smooth(s0,3,/edge_truncate)
    s0(nx0/2,nx0/2)=0.0
endfor
s0(where(s0 lt 0.0))=0.0

; display original power spectrum as generated above
window,0,xsize=nx0,ysize=nx0
loadct,33
tvscl,s0

; amplitude spectrum
a0=sqrt(s0)

; random phases for all components
ph0=randomu(seed,nx0,nx0)*2.0*!pi

; generate time series of images
window,1,xsize=nx0,ysize=nx0
loadct,0
imga=fltarr(nx1,nx1,nt0)
for kt=0,nt0-1 do begin
    pht=ph0-om0*dt0*kt
    ; complex amplitude array for inverse fourier transform
    f=a0*complex(cos(pht),sin(pht))
    ; add some random noise
    f=f+1.0e-4*complex(randomn(seed,nx0,nx0),randomn(seed,nx0,nx0))
    ; create image for this time step by inverse fourier transform
    imgt=real_part(fft(f,/center,/inverse))+1.0
    ; use only center part to avoid periodic boundary conditions (more realistic)
    imgt=imgt(0:nx1-1,0:nx1-1)
    ; save in layer of imga for this time step
    imga(*,*,kt)=imgt
    ; display
    imgd=bytscl(imgt,min=0.0,max=2.0)
    tv,congrid(imgd,nx0,nx0)
    ; wait 0.1 s, so we can see the moving wave patterns in window 1
    wait,0.1*dt0
endfor

; perform fourier transform and compute power spectrum
f1=fft(imga-mean(imga),/center)
; reverse because frequencies are upside down
f1=reverse(f1,3)
f1=real_part(f1*conj(f1))

; mean over all frequencies
f1m0=mean(f1(*,*,*          ),dimension=3)

; mean over all frequencies, positive frequencies only
f1m1=mean(f1(*,*,nt0/2:nt0-1),dimension=3)

; cross section of kx-omega plane along x axis
f1kxom=fltarr(nx1,nx1)
f1kxom(*,*)=f1(*,128,*)

stop

end