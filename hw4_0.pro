; oce 687 homework 4, 2020
; polich 12 apr 2020

pro hw4_0

; Window size for Fast Fourier Transform
ws=1024

; Produce a sinusoidal wave pattern by adding three sine functions:
; Create a sine wave – One pattern with an amplitude of 2.0 and 4 waves across the array in x direction.
amp=2.0
wav=4
x1=sin((wav*(2*!pi)/ws)*findgen(ws))*amp
; Create a sine wave – One pattern with an amplitude of 1.0 and 6 waves across the array in x direction.
amp=1.0
wav=6
x2=sin((wav*(2*!pi)/ws)*findgen(ws))*amp
; Create a sine wave – One pattern with an amplitude of 0.5 and 8 waves across the array in y direction.
amp=0.5
wav=8
y1=sin((wav*(2*!pi)/ws)*findgen(ws))*amp

; Generate an array of 1024×1024 pixels in IDL that has three sinusoidal wave patterns in it
; Add the two sine waves in the x direction, put them in the array
a0=fltarr(ws,ws)
for row=0,ws-1 do a0(*,row)=x1+x2
; Add the sine waves in the y direction to the array
for col=0,ws-1 do a0(col,*)=a0(col,*)+y1

;Check the display of the sine wave components
;window,0,xsize=ws,ysize=300
;loadct,0
;plot,x1,y1,title="Sine Wave: 2.0 x 4 waves"
;plot,x2,y2,title="Sine Wave: 1.0 x 6 waves"
;plot,x3,y3,title="Sine Wave: 0.5 x 8 waves"
;plot,a0(*,0),title="Sine Wave: X Axis Combined"
;plot,a0(0,*),title="Sine Wave: Y Axis"

; Display the array on the screen. It represents a simple simulated image of waves.
; Generate and display the simulated image correctly.
window,1,xsize=ws,ysize=ws,xpos=50,ypos=50,title="Simulated image of waves"
loadct,0
tvscl,a0


; Apply an FFT: transform this simulated wave image from the spatial to the frequency (complex) domain.
; Also shift the zero-frequency location to the center
fastft = fft(a0,/center)

; Compute the squared magnitude of the Fourier transform correctly.
; It represents an accurate power spectrum for this simple case.
magfft = abs(fastft)
sqfft = magfft^2

; Reduce to the center part of the power spectrum (25 pixels square)
d0=sqfft(ws/2-12:ws/2+12,ws/2-12:ws/2+12)
d0=rebin(d0,500,500)
; Display the center part of the power spectrum in a useful way such that:
; the different locations and magnitudes of its three main components are well visible.
window,2,xsize=500,ysize=500
loadct,33
tvscl,d0


; Discuss and provide correct explanations for the locations and relative magnitudes of:
; the three main components of the power spectrum.
; and how they make sense, given what you know about the input image.
; 
; In the center cut image displayed above, the three main components are visible
; 1) The strongest dots, the inner left/right pair, result from the component of x1
;    The x1 Sine Wave = 2.0 x 4 waves, which is both highest amplitude and lowest frequency
; 2) The second strongest dots, the outer left/right pair, result from the component of x2
;    The x2 Sine Wave = 1.0 x 6 waves, which is the second highest amplitude second lowest frequency
; 3) The weakest dots on the top/bottom, at center, result from the component of y1
;    The y1 Sine Wave = 0.5 x 8 waves, which is the lowest amplitude and the highest frequency
;
; The amplitude of each component results in the strength of their 'dot' patterns.
; If I change x1 amplitude to 2.0, then both x1 and x2 effect equal intensity on their relative dot patterns.
; Or if I change the y1 amplitude to 3.0, then the top/bottom pair of dots become most intense.
; 
; The frequency of each component results in the position of the dots relative to the axis.
; If I change the x1 frequency to 10 waves, then x1 is still the strongest component, but its position
; shifts from the inner left/right pair, to become the outer left/right pair, further out.
; The higher the frequency of the wave, the further away from the axis it's power spectrum appears, and vice versa.

stop


; Additional Tests:

; Log-scale the power spectrum, otherwise the center component is too predominant to see other features
logfft = alog10(sqfft)

; Display the log-scaled power spectrum as a 3D surface
; x-axis and y-axis will have as labels the number of pixels in each dimension
; According to IDL procedures here: https://www.harrisgeospatial.com/docs/FFTReduceBackgroundNoise.html
surfacefft = surface(logfft,axis_style=1,ztitle="FFT Power Spectrum Surface",color="red")
; Add a z-axis, showing values at two corners
surfaceax = axis('Z',location=[0,ws,0])
; The lowest frequencies are indicated by the sharp distinct peak in the center of the 3D plot

; [assignment note] Also tried this function, but not sure how to use and interpret the resulting plot;
; the 3d surface method above seems more understandable. Leaving all code here for completeness.
freq1 = ws/4
freq2 = ws/6
freq3 = ws/8
; Input is an N-element array >> Result is a vector of length N/2 + 1
; e.g. fps has length = (1024*1024)/2+1 = 524,289
fps = FFT_PowerSpectrum(a0,freq=freq)
p = plot(freq,fps(0:10240),/ylog,xtitle='Frequency')

; The following line would transform the FFT back into the spatial domain
; inversefft = real_part(fft(fastft,/inverse,/center))
; However, the re-transformed array is not identical to the original
; print,array_equal(a0,inversefft) = 0
; max(a0) = 3.3560584
; max(inversefft) = 3.3560581
; min(a0) = -3.3560581
; min(inversefft) = -3.3560581
; mean(a0) = -1.2888131e-007
; mean(inversefft) = -1.1061196e-007

stop

end