; oce 687 homework 2, 2020
; based on midterm exam 2016
; same as hw2 2017, hw1 2018,2019
; roro 16 feb 2016 .. 14 feb 2020

pro hw2_0

read_jpeg,'C:\roland\oce687\2020\hw2\hw2_a.jpg',ta
read_jpeg,'C:\roland\oce687\2020\hw2\hw2_b.jpg',tb

window,0,xsize=1000,ysize=500
loadct,0

; (here you can stop and have a look at ta and tb if you want)

; difference image
d=1.0*tb-1.0*ta

; (here you can stop and have a look at d; you will see that it is a little noisy due to jpeg-related quality losses)

; make change mask
m0=bytarr(1000,500)
m0(where(d lt -100.0))=1   ; initial mask; to be improved in the following steps
; expand
m1=m0
w=where(m1 eq 1,c1)
repeat begin
   c0=c1
   s=smooth(1.0*m1,3,/edge_truncate)
   m1(where(s gt 0.2 and d lt -20.0))=1
   w=where(m1 eq 1,c1)
endrep until c1 eq c0

; (here you can stop and compare the initial mask m0 and the improved mask m1 if you want)

loadct,33

; identify clusters
m2=bytarr(1000,500)   ; this will become a new version of m1 in which different pixel clusters have different numbers
index=0
repeat begin
   index=index+1
   mi=bytarr(1000,500)   ; this will become a temporary mask just for the next cluster we are working on

   ; in the following line, this happens:
   ; - where(...) finds all pixels that are 1 in mask m1, but that are still 0 in mask m2
   ; - then (where(...))(0) picks just the first element (element 0) of where(...), i.e. just one single pixel where m1 is 1 and m2 is 0
   ; - this pixel becomes the start pixel for the expansion of the next pixel cluster
   mi((where(m1 eq 1 and m2 eq 0))(0))=1

   ; now expand the cluster around its start pixel
   w=where(mi eq 1,c1)
   repeat begin
      c0=c1
      s=smooth(1.0*mi,3,/edge_truncate)
      mi(where(s gt 0.1 and m1 eq 1))=1

      ; display completed clusters and the cluster we are working on in colors
      tv,50*(m2+mi*index)

      ; find out how many elements the cluster has; stop the expansion when it is not growing anymore
      w=where(mi eq 1,c1)
   endrep until c1 eq c0
   m2=m2+mi*index
   
   ; check if there are remaining pixels in m1 that do not yet belong to a numbered cluster in m2
   w=where(m1 eq 1 and m2 eq 0,c0)
endrep until c0 eq 0

; (insert your additional code here)




stop

end