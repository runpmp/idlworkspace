; oil monitoring demo 2020
; polich 27 feb 2020

pro oil0

;goto,step2
goto,step3

t0=read_tiff('C:\Users\Admin\Documents\IDLWorkspace\20100512155632_ENV__SVV_01_CSTARS.tif')
t0=rotate(t0,2)

; median filtered image
m0=median(t0,11)

; cut size to 5500x6600
t0=t0(5:5504,75:6674)
m0=m0(5:5504,75:6674)

; save these products into binary files for further use
openw,1,'C:\Users\Admin\Documents\IDLWorkspace\t0.dat'
writeu,1,t0   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1
openw,1,'C:\Users\Admin\Documents\IDLWorkspace\m0.dat'
writeu,1,m0   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1

;stop
;step2:

; read level 0 arrays back into memory
t0=fltarr(5500,6600)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\t0.dat'
readu,1,t0   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1
m0=fltarr(5500,6600)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\m0.dat'
readu,1,m0   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1

; replace bright spikes in t0 by median value from m0
t1=t0
w=where(t0 gt 2.0*m0)
t1(w)=m0(w)

; normalize intensities by dividing by mean profile
; better to smooth and avoid producing artifacts
p1=mean(t1(*,0:2499),dimension=2,/double)
p2=p1
for k=0,399 do p2=smooth(p2,3,/edge_truncate)
t2=t1
for i=0,5499 do t2(i,*)=t2(i,*)/p2(i)

openw,1,'C:\Users\Admin\Documents\IDLWorkspace\t2.dat'
writeu,1,t2   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1

stop

step3:

;read level 2 array back into memory
t2=fltarr(5500,6600)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\t2.dat'
readu,1,t2   ; unformatted, writes binary data from memory into file << good to know how to work with binary files
close,1

window,0,xsize=1100,ysize=1100

; first simple oil mask

; smooth image very much to reduce speckle noise
;t3=t2
;for k=0,9 do t3=smooth(t3,11,/edge_truncate)
;o0=fltarr(5500,6600)
;o0(where(t3 lt 0.5))=1.0


; advanced oil mask

o1=fltarr(5500,6600)
o1(where(t2 lt 0.5))=1.0

; primitive land mask: set everything above line 4000 to 0.0
o1(*,6000:6599)=0.0
; try cmds
; tvscl,rebin(o1,1100,1100)
; tvscl,congrid(o1,1000,1000)
; tvscl,rebin(t2,1100,1100)<2.0

s=o1
for k=0,1 do s=smooth(s,3,/edge_truncate)
o2=o1
o2(where(s lt 0.99))=0.0


; expand cleaned mask to include all dark neighbor pixels again
o3=o2
w=where(o3 gt 0.5,c1)
repeat begin
  c0=c1
  s=smooth(o3,3,/edge_truncate)
  w=where(s gt 0.2 and t2 lt 0.5,c1)
  o3(w)=1.0
  tvscl,o3(3000:4099,3000:4099)
  print,c0,c1,c1-c0
endrep until c1 eq c0

; eliminate remaining small holes
s=smooth(o3,3,/edge_truncate)
o3(where(s gt 0.2))=1.0
tvscl,o3(3000:4099,3000:4099)

; could adjust initial mask by select darker pixels first
; could use other techniques to smooth border, eg. set threshold of 0.5 to apply smooth

;good to alway check for an image:
;IDL> print,max(t0)
;409.279
;IDL> print,mean(t0)
;0.286442

;window,0,xsize=1000,ysize=1000
;tvscl,congrid(t0,1000,1000)<0.5
;tvscl,rebin(m0,550,660)<0.5
;tvscl,rotate(congrid(t0,1000,1000),2)<0.5

stop

end