;change detection demo
;version 1.0
;polich 94 feb 2929

pro change0_2020

read_jpeg,'C:\Users\evo\Documents\IDLWorkspace\ta.jpg',ta
read_jpeg,'C:\Users\evo\Documents\IDLWorkspace\tb.jpg',tb

;print 1L*1150*750  -- force IDL to convert type to long

sx=(size(ta))(1)
sy=(size(ta))(2)

window,0,xsize=sx,ysize=sy

;simple approach: use ta, tb as color channels of 3-color image
;t3=bytarr(sx,sy,3)
;t3(*,*,0)=ta(*,*)
;show colors in red/orange
;t3(*,*,1)=tb(*,*)
;t3(*,*,2)=tb(*,*)
;show colors in blue/cyan
;t3(*,*,1)=ta(*,*)
;t3(*,*,2)=tb(*,*)
;tv,t3,true=3

; make combined bright image for display purposes
tc=ta
w=where(tb gt ta,c)
if c gt 0 then begin
  ; avoid evaluating unless necessary
  tc(w)=tb(w)
endif

; make three-color version
t3=bytarr(sx,sy,3)
t3(*,*,0)=tc(*,*)
t3(*,*,1)=tc(*,*)
t3(*,*,2)=tc(*,*)

; difference between tb and ta
td=1.0*tb-1.0*ta
;tvscl,td
;tv,bytscl(td) --- same thing
;print,min(td),max(td)

; highlight in red what was removed between ta and tb
;w=where(td lt -100.0,c)
;if c gt 0 then begin
;  t1=t3(*,*,1)
;  t1(w)=0
;  t3(*,*,1)=t1(*,*)
;  t1=t3(*,*,2)
;  t1(w)=0
;  t3(*,*,2)=t1(*,*)
;endif

; highlight in green what was removed between ta and tb
w=where(td gt 100.0,c)
if c gt 0 then begin
  t1=t3(*,*,1)
  t1(w)=0
  t3(*,*,0)=t1(*,*)
  t1=t3(*,*,2)
  t1(w)=0
  t3(*,*,2)=t1(*,*)
endif

;tv,t3,true=3

; even more advanced approach


; mask showing pixels where something has been removed
ma0=bytarr(sx,sy)
ma0(where(td lt -100.0))=1

ma1=bytarr(sx,sy)
ma1(where(td lt -100.0 and ta gt 180))=1

; reduce to clusters of at least 3x3 pixels
s=smooth(1.0*ma1,3,/edge_truncate)
ma2=ma1
ma2(where(s lt 0.9))=0
; expand clusters that have survived to all connected bright pixels

ma3=ma2
repeat begin
  s=smooth(1.0*ma3,3,/edge_truncate)
  w=where(ma3 eq 0 and s gt 0.1 and td lt -20.0 and ta gt 127,c)
  if c gt 0 then begin
    ma3(w)=1
  endif
  print,c
  tvscl,ma3
endrep until c eq 0

;test
;tv,ma1*128+ma2*127
;loadct,33
;tv,ma1*128+ma2*127

; mask showing pixels where something has been removed
mb0=bytarr(sx,sy)
mb0(where(td gt 100.0))=1

mb1=bytarr(sx,sy)
mb1(where(td gt 100.0 and tb gt 180))=1

; reduce to clusters of at least 3x3 pixels
s=smooth(1.0*mb1,3,/edge_truncate)
mb2=mb1
mb2(where(s lt 0.9))=0
; expand clusters that have survived to all connected bright pixels

mb3=mb2
repeat begin
  s=smooth(1.0*mb3,3,/edge_truncate)
  w=where(mb3 eq 0 and s gt 0.1 and td gt 20.0 and tb gt 127,c)
  if c gt 0 then begin
    mb3(w)=1
  endif
  print,c
  tvscl,mb3
endrep until c eq 0

stop

end