; wave retrieval demo
; polich 26 mar 2020

pro wavetest_2020

t0=fltarr(15000,13000)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\image_15000x13000.flt'
readu,1,t0
close,1

t1=rebin(t0,1500,1300)
window,0,xsize=1500,ysize=1300
loadct,0
tvscl,10.0*alog10(t1)>(-25.0)<(-5.0)

; pixel size in meters
res=0.75

; window size for fft
ws=2048

;tij=t0(0:ws-1,0:ws-1)
;tij=tij/mean(tij,/double)
;tij=tij-1.0
;fij=fft(tij,/center)

; calculate power spectrum (square of the magnitude) of the Fourier transform
;sij=real_part(fij*conj(fij))
;tvscl,congrid(sij(924:1124,924:1124),804,804)

; define tapering function
tap=fltarr(ws,ws)
for i=0,ws-1 do for j=0,ws-1 do tap(i,j)=sin(!pi/(ws-1)*i)*sin(!pi/(ws-1)*j)

s0=fltarr(ws,ws)
s1=fltarr(ws,ws)
; loop for overlapping windows (40)
for i=0,9 do for j=0,3 do begin
  tij=t0(i*ws/2:i*ws/2+ws-1,j*ws/2:j*ws/2+ws-1) ; always gives a 2048 window size
  ; normalization
  tij=tij/mean(tij,/double)
  ; subtraction of mean
  tij=tij-1.0
  ; Fourier transform
  fij=fft(tij,/center)
  ; power spectrum (not exactly, but almost)
  sij=real_part(fij*conj(fij))
  s0=s0+sij
  ; Fourier transform with tapering
  ; It is standard practice to multiply the data windows by a taper before performing a Fourier transform.
  ; The taper consist in a function smoothly decaying to zero near the ends of each window, aimed at
  ; minimizing the effect of the discontinuity between the beginning and end of the time series.
  ; Although spectral leakage cannot be prevented it can be significantly reduced by changing the shape
  ; of the taper function in a way to minimize strong discontinuities close to the window edges.
  fij=fft(tij*tap,/center)
  ; power spectrum (not exactly, but almost)
  sij=real_part(fij*conj(fij))
  s1=s1+sij
endfor ; 1 endfor works for both loops
s0=s0/40.0
s1=s1/40.0

; reduce to center of spectrum (1024 x 1024)
s0=s0(ws/2-50:ws/2+50,ws/2-50:ws/2+50)
s1=s1(ws/2-50:ws/2+50,ws/2-50:ws/2+50)

; display image spectrum
window,1,xsize=808,ysize=808
loadct,33
tvscl,congrid(s0,808,808)

; display image spectrum
window,2,xsize=808,ysize=808
loadct,33
tvscl,congrid(s1,808,808)

; display wave image
;window,3,xsize=1024,ysize=1024
;loadct,33
;tvscl,rebin(tij,1024,1024)

stop


; wavenumber arrays kx, ky
; [note] element (1025 x 1024) represents 1 oscillation, +1 another oscillation, etc.
dk=2.0*!pi/(res*ws)
kx=fltarr(101,101)
for i=0,100 do kx(i,*)=dk*(i-50)
ky=fltarr(101,101)
for j=0,100 do ky(*,j)=dk*(j-50)

; magnitude of wavenumber
ka=sqrt(kx^2+ky^2)
; [note] wavelength = 2pi/ka

; cut out unwanted spectral energy around center
; (all energy at wavelengths of 450 m or longer)
s2=s1
s2(where(ka le 2.0*!pi/450.0))=0.0

; remove spectral energy of components moving west- or southward
s2(where(kx le 0.0))=0.0
s2(where(ky le 0.0))=0.0

; remove spectral energy less than 1/10 of speak energy
s2(where(s2 lt 0.1*max(s2)))=0.0

tvscl,congrid(s2,808,808)

; compute power-weighted peak wavenumber vector
kpx=total(s2*kx)/total(s2)
kpx=total(s2*ky)/total(s2)

; can also calculate wave slope - dimensionless quanity
; image spectrum is also dimensionless / divide that by mtf^2 >>> then divide by ka^2
; both quantities can correlated with each other with modulation transfer function
; construct mtf to determine it's shape and location of spectral peak
; then can compute wave height spectrum, to estimate wave height

stop

end