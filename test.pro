;test file for oce687
;version 1.0
;polich 21 jan 2020

pro test

;use descriptive variable names
a0=fltarr(200,100)
a0(0:19,10:49)=128.0
a0(50:99,30:69)=160.0
a0(10:59,*)=a0(10:59,*)+60.0
;idl will decide variable type based on its entry
;floats and doubles available


window,0,xsize=800,ysize=400
loadct,33
;tvscl,congrid(rebin(a0,50,25),800,400)
tvscl,congrid(a0,800,400)
;tv may not strength/contrast (as tvscl)


;multiple arrays: eg. land mask of original (to make black)

; congrid resamples the image
    ; will look just as noisy as original image, but with 1/16th pizels
; rebin interpolates/averages between original values
    ; picks every 4th value - can help better estimate sigma-0 in noisy image

;window,1,xsize=800,ysize=500,xpos=1100,ypos=450
;plot,a0(*,40)

;a0(where(a0 eq max(a0)))=20.0
w=where(a0 eq max(a0))
a0[w]=20.0
for j=0,99 do a0(*,j)=a0(*,j)+1.0*j
a0(150,50)=10000.0
for k=0,9 do a0=smooth(a0,3,/edge_truncate)
;tvscl,congrid(a0,800,400)
;tvscl,rotate(congrid(a0,800,400),2)
tvscl,reverse(congrid(a0,800,400),1)

for k=0,999 do tvscl,rot(congrid(a0,800,400),k,1.0,400,200,/pivot)

stop

end