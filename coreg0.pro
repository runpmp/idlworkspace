; coregistration demo 2020
; ; polich 09 apr 2020
 
pro coreg0

tf0=complexarr(1024,1024)
; (read in the file on home system)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\tf.cpx'
readu,1,tf0
close,1

ta0=complexarr(1024,1024)
; (read in the file on home system)
openr,1,'C:\Users\Admin\Documents\IDLWorkspace\ta.cpx'
readu,1,ta0
close,1

; TerraSAR-X images approximately 0.1 ms apart

pf0=real_part(tf0*conj(tf0))
pa0=real_part(ta0*conj(ta0))

window,0,xsize=1024,ysize=1024

d3=bytarr(3,1024,1024)
d3(0,*,*)=(congrid(bytscl(sqrt(pf0(400:655,400:655))),1024,1024))(*,*)
d3(1,*,*)=(congrid(bytscl(sqrt(pa0(400:655,400:655))),1024,1024))(*,*)
tv,d3,true=1

; to move first fourier component by one pixel, must shift by 2pi
; to move second fourier component by one pixel, must shift by 4pi
; to move third fourier component by one pixel, must shift by 6pi

; for every pixel you have a real and imaginary value contained
; essentially twisting a spiral of the imaginary and real domains, bring some parts into each
; still only 1024 values, but they are shifted
; e.g. shift 1/100 of pixel -- still obtain an accurate image, without degradation
; the shift works correctly only in the fourier domain (they must have different phase shifts)
; you will see the sum all the fourier shifts

ffta=fft(ta0,/center)
ampa=sqrt(real_part(ffta*conj(ffta)))
phsa=atan(ffta,/phase)

; phase ramp array
; change phase of fourier component by amount corresponding to 1/100 pixel length in y direction
dphy=fltarr(1024,1024)
for j=0,1023 do dphy(*,j)=0.02*!pi/1024.0*(j-512)

; shift test
cor=fltarr(401)
window,1,xsize=1024,ysize=400
for k=0,400 do begin
  phsak=phsa-dphy*(k-200)
  fftak=complex(ampa*cos(phsak),ampa*sin(phsak))
  tak=fft(fftak,/inverse,/center) ; inverts back from fourier to spatial domain
  pak=real_part(tak*conj(tak))
  d3(1,*,*)=(congrid(bytscl(sqrt(pak(400:655,400:655))),1024,1024))(*,*)
  cor(k)=correlated(pf0,pak)
  wset,0
  tv,d3,true=1
  wset,1
  plot,indgen(401)-200,cor
  print,k-200,cor(k)
endfor

;print,where(cor eq max(cor)) = 241

stop

end