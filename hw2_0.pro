; oce 687 homework 2, 2020
; based on midterm exam 2016
; same as hw2 2017, hw1 2018,2019
; roro 16 feb 2016 .. 14 feb 2020
; identifies five clusters of pixels that have changed between images "a" and "b".
;
; modified by polich 20 feb 2020

pro hw2_0

; This is another change detection example between two image files: hw2_a.jpg and hw2_b.jpg
; (read in the files on home system)
read_jpeg,'C:\Users\Admin\Documents\IDLWorkspace\hw2_a.jpg',ta
read_jpeg,'C:\Users\Admin\Documents\IDLWorkspace\hw2_b.jpg',tb
; (read in the files on portable system)
;read_jpeg,'C:\Users\evo\Documents\IDLWorkspace\hw2_a.jpg',ta
;read_jpeg,'C:\Users\evo\Documents\IDLWorkspace\hw2_b.jpg',tb
; (read in the files on instructor system)
;read_jpeg,'C:\roland\oce687\2020\hw2\hw2_a.jpg',ta
;read_jpeg,'C:\roland\oce687\2020\hw2\hw2_b.jpg',tb

window,0,xsize=1000,ysize=500
loadct,0

; (here you can stop and have a look at ta and tb if you want)

; difference image
d=1.0*tb-1.0*ta

; (here you can stop and have a look at d; you will see that it is a little noisy due to jpeg-related quality losses)


; make change mask
m0=bytarr(1000,500)   ; 500,000 array elements
m0(where(d lt -100.0))=1   ; initial mask; to be improved in the following steps
; expand
m1=m0
w=where(m1 eq 1,c1)
repeat begin
   c0=c1
   s=smooth(1.0*m1,3,/edge_truncate)
   m1(where(s gt 0.2 and d lt -20.0))=1
   w=where(m1 eq 1,c1)
endrep until c1 eq c0

; (here you can stop and compare the initial mask m0 and the improved mask m1 if you want)



loadct,33 ; load colors for later drawing

; identify clusters
m2=bytarr(1000,500)   ; this will become a new version of m1 in which different pixel clusters have different numbers

m3=bytarr(1000,500)   ; this will become a new version of m2 in which only the biggest pixel cluster is identified and saved
max_idx=10   ; constant representing the maximum number of clusters to evaluate (with the sample images, it's only 5)
clstrs=intarr(1,max_idx) ; simple list of cluster sizes, if any exist this will begin with 1st index position
big_idx=0   ; index of biggest cluster in list clstrs


index=0
repeat begin
   index=index+1

   mi=bytarr(1000,500)   ; this will become a temporary mask just for the next cluster we are working on

   ; in the following line, this happens:
   ; - where(...) finds all pixels that are 1 in mask m1, but that are still 0 in mask m2
   ; - then (where(...))(0) picks just the first element (element 0) of where(...), i.e. just one single pixel where m1 is 1 and m2 is 0
   ; - this pixel becomes the start pixel for the expansion of the next pixel cluster
   mi((where(m1 eq 1 and m2 eq 0))(0))=1

   ; now expand the cluster around its start pixel
   w=where(mi eq 1,c1)
   repeat begin
      c0=c1
      s=smooth(1.0*mi,3,/edge_truncate)
      mi(where(s gt 0.1 and m1 eq 1))=1

      ; display completed clusters and the cluster we are working on in colors
      tv,50*(m2+mi*index)

      ; find out how many elements the cluster has; stop the expansion when it is not growing anymore
      w=where(mi eq 1,c1)
   endrep until c1 eq c0
   m2=m2+mi*index

   ; just in case indexed clusters grew larger than set constant max_idx (with the sample images this shouldn't be triggered)
   if index gt max_idx-1 then begin
      print,'number of clusters greater than max_idx'
      stop
   endif
   clstrs(index)=c1   ; add the final size of the cluster in pixels (after its growth) to array at index, c1 = n_elements(w)

   ; test if the current cluster mi is the biggest so far
   if clstrs(index) gt clstrs(big_idx) then begin
      big_idx=index ;  note index (and also preserve relative color intensity)
      m3=mi ; the current cluster mi is bigger, replace m3 with mi
   endif

   ; check if there are remaining pixels in m1 that do not yet belong to a numbered cluster in m2
   w=where(m1 eq 1 and m2 eq 0,c0)
   ;print,'remaining elements: ',n_elements(w)
   ;wait,5  ; pause to identify cluster by index
endrep until c0 eq 0



; (insert your additional code here)

; a) Please add code that prints the sizes of the five clusters (numbers of pixels)
; in the IDL console window and identifies the biggest cluster. [1.5%]

desc_str=''  ; description of cluster
for idx=1,index do begin ; start from idx 1 until last clusters index
   if idx eq big_idx then begin
      desc_str=' pixels in size <<<<<----------- biggest cluster' ; identifies the biggest cluster
   endif else begin
      desc_str=' pixels in size'
   endelse
   print,'cluster # ',idx,' =',clstrs(idx),desc_str ; prints the sizes of the clusters (5 should be shown in this example)
endfor

; [assignment note] verified the cluster sizes are accurate, although visually clusters #4 and #5 look as big as #1 (an illusion)



; b) Then add code to display the shape of this biggest cluster in an IDL window
; (without the other clusters). [1.5%]

if big_idx > 0 then begin
   window,1,xsize=1000,ysize=500 ; create a second IDL window
   loadct,33  ; display the biggest cluster in color as above
   tv,50*(m3*big_idx) ; display the shape of the biggest cluster in m3 (boosting the color intensity by 50 and by index as above)
endif

; [assignment note] array m3 set earlier contains only the biggest cluster elements as a mask with value 1, other elements have value 0



; c) Then add code to construct a new grayscale image that keeps the changes from image "a" to image "b"
; in this biggest cluster of pixels, but shows the original image "a" everywhere else. [2.0%]

m4=ta    ; in array m4 for new grayscale image, show the original image "a" everywhere else
w2=where(m3 eq 1,big_idx) ; new vector w2 that contains the one-dimensional subscripts of the nonzero elements of m3
for idx=0,big_idx-1 do begin
  m4[w2[idx]]=tb[w2[idx]]    ; where cluster mask m3 is on, keep the changes from image "a" to image "b"
endfor

window,2,xsize=1000,ysize=500 ; create a third IDL window (in grayscale)
loadct,0
tv,m4   ; show the original image "a" plus changes from image "a" to image "b" in the biggest cluster of pixels


; [assignment note] nested for loops below work correctly but simpler IDL code was possible (shown for completeness)
;m5=bytarr(1000,500)  ; array m5 to hold a new grayscale image
;for i=0,1000-1 do begin
;   for j=0,500-1 do begin
;      if m3[i,j] eq 0 then begin
;         m5[i,j] = ta[i,j]    ; where cluster mask m3 is off, show the original image "a" everywhere else 
;      endif else begin
;         m5[i,j] = tb[i,j]    ; where cluster mask m3 is on, keep the changes from image "a" to image "b"
;      endelse
;   endfor
;endfor
;window,3,xsize=1000,ysize=500 ; create a fourth IDL window (in grayscale)
;loadct,0
;tv,m5   ; show the original image "a" plus changes from image "a" to image "b" in the biggest cluster of pixels


stop

end