; oce 687 homework 3, 2020
; polich 28 mar 2020
;
; [assignment note] Some smaller resolution windows are chosen due to the resolution of my monitor.

pro hw3_0

  ; steps to facilitate reviewing each step of the assignment separately
  ;goto,step2
  ;goto,step3

  ; This is a plain binary file containing a floating-point array of 3000 × 6000 pixels
  ; RADARSAT-2 ScanSAR image of a typhoon approaching the Philippines provided as a dB image
  b0=fltarr(3000,6000)
  ; (read in the file on home system)
  openr,1,'C:\Users\Admin\Documents\IDLWorkspace\hw3.dat'
  ; (read in the file on instructor system)
  ;openr,1,'C:\roland\oce687\2020\hw3\hw3.flt'
  readu,1,b0
  close,1

  ; median filtered image
  ;m0=median(b0,2) ; size=2, two-dimensional neighborhood to be used for the median filter.
  ; [assignment note, above] Median filtering was preferred for use with the smallest window size = 2.
  ; This is a much smaller value than used in the oil spill class application (for a mask).
  ; The goal here was to preserve as much detail as possible, while eliminating some spikes in the data that affect intensity.
  ; However, it was decided not to use a median filter, because when used it will produce additional sampling and affect the later 'surprise'.

  ; save this product into binary file for further use
  ;openw,1,'C:\Users\Admin\Documents\IDLWorkspace\hw3_m0.dat'
  ;writeu,1,m0   ; unformatted, writes binary data from memory into file
  ;close,1

  ;stop
  step2:

  ; read level 0 arrays back into memory
  ;b0=fltarr(3000,6000)
  ; (read in the file on home system)
  ;openr,1,'C:\Users\Admin\Documents\IDLWorkspace\hw3.dat'
  ; (read in the file on instructor system)
  ;openr,1,'C:\roland\oce687\2020\hw3\hw3.flt'
  ;readu,1,b0
  ;close,1
  ;m0=fltarr(3000,6000)
  ; (read in the file on home system)
  ;openr,1,'C:\Users\Admin\Documents\IDLWorkspace\hw3_m0.dat'
  ; (read in the file on instructor system)
  ;openr,1,'C:\roland\oce687\2020\hw3\hw3_m0.flt'
  ;readu,1,m0   ; unformatted, writes binary data from memory into file
  ;close,1

  t0=b0
  ; replace intense spikes in b0
  ;w=where(b0 gt 2.0*m0) ; used with median filtering and values from m0
  ;t0(w)=m0(w)
  ; [assignment note] Results in lower value for max(t0) < max (b0) and a slightly greater value for mean(t0) > mean(b0)

  ; When you read it into IDL and display it, you will notice that it has a bad processing artifact
  ; (which I produced for the purpose of this exercise; the original data quality was good).
  ;
  ; [assignment note] Yes, processing artifacts are observed - mainly horizontal banding or striping.

  ; mark swath line for estimating intensity (avoid land, in right side of image with most contrast)
  p0=t0
  p0(2300,*)=min(p0)  ; left column border -- right column border is right edge
  ; [assignment note] We will take 700 pixels to the right side of this line to produce the mean intensity below.
  ; Overall, this column of 700 pixels seems to be the most uniform in intensity (clouds and ocean, with few dark patches),
  ; and produced the most even mean intensity profile. Multiple other swath widths were tested without as good of output
  ; (including as narrow as 1 pixel, and the entire width of image), and they produced 'brighter' or 'darker' horizontal swaths.

  ; show the plotted image with intensity columns in a smaller window, for review purposes
  s0=rebin(p0,300,600)
  window,0,xsize=300,ysize=600
  loadct,0
  tvscl,s0


  ; (a)
  ; Please try to remove this artifact. Apply what we have done for the incidence angle normalization of the oil spill image.
  ; Try to derive and apply your correction using: (a) the dB image
  ; Test method (a) correctly (see from you code and comment lines)
  ;
  ; calculate a mean intensity profile of the swath/column
  ; * = vertical, 6000 pixels vertical, average in 1st dimension
  ; horizontal will have the mean value over 700 pixels
  p1=mean(t0(2300:2999,*),dimension=1,/double) ; use double number here for more accuracy at dB scale
  ; [assignment note] Unlike in the oil spill example, smoothing of the mean couldn't be done or the strong artifact couldn't be removed correctly.

  ; plot and display this mean intensity function (over the 700 pixels column)
  window,1,xsize=700,ysize=200
  loadct,0
  plot,p1,thick=3.0,title="Mean Intensity (dB) with Artifact"

  ; [assignment note] Some sort of sine wave appears transposed over the intensity profile, producing the image banding artifact.
  ; The intensity profile plot shows a sine wave with amplitude approximately -2.5 db (in image originally provided as a dB image).

  ; print the mean intensity profile of the swath with artifact
  print,'mean (p1) ',mean(p1)

  col=fltarr(6000)
  t2=t0
  for idx=0,2999 do begin
     col=t2(idx,*) ; set the next column
     for idy=0,5999 do begin
        ; normalize intensities by subtracting the mean intensity
        ; for the dB image, subtracting the mean intensity is necessary to remove it, including the artifact
        col(idy)=col(idy)-p1(idy)
     endfor
     ; it's possible to apply boxcar smoothing to each pixel column -- set width/smoothing window size
     ; col=smooth(col,3,/edge_truncate)
     ; [assignment note, above] Smoothing is not used, as in the oil spill class example, because the later 'surprise' should affect the intensity.
     t2(idx,*)=col ; save this column
  endfor

  ; display the corrections in a smaller window for review purposes:
  ;s1=rebin(t2,300,600)  ; show this corrected dB image
  ;s1=rebin(t0-t2,300,600) ; or show the difference (artifact pattern) removed from the original dB image
  ;window,2,xsize=300,ysize=600
  ;loadct,0
  ;tvscl,s1

  p2=mean(t2(2300:2999,*),dimension=1,/double) ; calculate/plot mean as double number for more accuracy at dB scale
  
  ; print the new mean intensity profile of the corrected swath/column
  print,'mean (p2) ',mean(p2)
  
  ; plot and display the corrected function (over the 700 pixels column)
  window,3,xsize=700,ysize=200
  loadct,0
  plot,p2,thick=3.0,title="Corrected Wave (dB)"
  ; [assignment notes] The plot appears to show a much smoother intensity profile. Visually, the sine wave artifact is removed.
  ; The only remaining artifact in the image are some dimly visible horizontal scan lines that resulted from the mean calculation.
  ; I was not able to remove all of scan lines: only to shift them around by using the mean calculation with a different column or width.
  ; The main reason that smoothing was preferred above was for its improvement in intensity profile: a strange intensity distribution is removed.
  ; Visually, no real difference was observed when using boxcar smoothing versus not using it (nor bigger or small smoothing window sizes).
  ; However, after realizing the effect of the later 'surprise' on the mean intensity, such that it should present another image artifact,
  ; I decided to deactivate the smoothing, although for the dB results it doesn't modify sampling of the ghost image such as for the linear results.
  ; Instead it skews the mean intensity profile, as values oscillate roughly around zero: due to the values in dB of the mixed in ghost image.

  
  ; (b)
  ; Please try to remove this artifact. Apply what we have done for the incidence angle normalization of the oil spill image.
  ; Try to derive and apply your correction using: (b) the linear image.
  ; Test method (b) correctly (see from you code and comment lines)
  
  ; create the linear domain of the original dB image
  d0=10^(t0/10)
  ; show this linear image in a smaller window for review purposes
  ;s2=rebin(d0,300,600)
  ;window,4,xsize=300,ysize=600
  ;loadct,0
  ;tvscl,s2

  ; calculate a mean intensity profile of the swath again (for linear domain)
  ; * = vertical, 6000 pixels vertical, average in 1st dimension
  ; horizontal will have 700 pixels with average value
  p3=mean(d0(2300:2999,*),dimension=1) ; the 'double' keyword doesn't need to be used for linear mean, unless to avoid float calculation errors
  ; [assignment note, above] This seemed to be suggested by the specifications (to calculate the same-sized swath width in linear).
  ; Other values were also tested (commented out below) to infer their effect on both the visual image and the image intensity.
  ;p3=10^(p1/10) ; this seemed to work best, converting the dB mean also into linear domain results in: mean (t2) ≈ mean (t3)
  ;p3=mean(d0(2999:2999,*),dimension=1)  ; narrowing the new mean calculation to 1 pixel produces a very bad result
  ;p3=mean(d0(0:2999,*),dimension=1)  ; increasing the new mean calculation to the whole width produces a worse result
  ; Calculating the mean in the linear domain across each entire vertical row (including land) would result in a skewed higher intensity.

  ; plot and display the mean intensity function
  window,5,xsize=700,ysize=200
  loadct,0
  plot,p3,thick=3.0,title="Mean Intensity (Linear) with Artifact"

  ; [assignment note] Again, some kind of sine wave appears transposed over the intensity profile, producing the image banding artifact.
  ; The intensity profile plot shows a sine wave with amplitude ranging from approximately 0.1 to 0.2 (in linear image derived from original dB image).
  ; This sine wave artifact appears more variable in the linear domain than in the dB domain with greater amplitude between 1500-4000 pixels vertical.
  
  ; print the mean intensity profile of the swath/column with artifact
  print,'mean (p3) ',mean(p3)

  ; also create and display a rotated version of the linear swath just to compare horizontally with p3 above
  ; [assignment note] The troughs of the intensity plot correspond with 'dark' areas; the peaks correspond with light areas.
  ;r0=rotate(d0,3)
  ;r0=r0(*,0:699)
  ;s3=rebin(r0,600,70)
  ;window,6,xsize=600,ysize=70,title="Swath (Linear) with Artifact"
  ;loadct,0
  ;tvscl,s3

  col=fltarr(6000)
  d1=d0
  for idx=0,2999 do begin
    col=d1(idx,*) ; set the next column
    for idy=0,5999 do begin
      ; normalize intensities by dividing by the mean intensity
      ; for the linear image, dividing by the mean intensity is necessary to remove it, including the artifact
      col(idy)=col(idy)/p3(idy)
    endfor
    ; it's possible to apply boxcar smoothing to each pixel column -- set width/smoothing window size
    ;col=smooth(col,3,/edge_truncate)
    ; [assignment note, above] Smoothing is not used, as in the oil spill class example, because when used here it affects the later 'surprise'
    d1(idx,*)=col ; save this column
  endfor
  
  ; show this corrected linear image in a smaller window for review purposes
  ;s4=rebin(d0,300,600)
  ;window,7,xsize=300,ysize=600
  ;loadct,0
  ;tvscl,s4
  
  p4=mean(d1(2300:2999,*),dimension=1)
  
  ; print the mean intensity profile of the corrected swath/column
  print,'mean (p4) ',mean(p4)

  ; plot and display the corrected function
  window,8,xsize=700,ysize=200
  loadct,0
  plot,p4,thick=3.0,title="Corrected Wave (Linear)"
  ; [assignment note] The plot now appears to show a strange mean intensity profile in the linear domain (discussed below).
  ; The prominent sine wave artifact has been successfully removed, although another strange artifact appears now.

  ; create a new dB image of d1 after corrections, convert back from linear
  t3=10.0*alog10(d1)

  ; print the full mean intensity of both corrected images in dB
  ; [assignment note] The intensity of t3 (corrected in linear) is slightly lower than in t2 (corrected in dB).
  print,'mean (t2) ',mean(t2)
  print,'mean (t3) ',mean(t3)

  ; Try to display both versions of the corrected image as dB images again to inspect their quality and see the difference.

  ; show the dB-corrected dB image in a smaller window for review purposes
  s5=rebin(t2,300,600)
  window,9,xsize=300,ysize=600,title="Corrected in dB"
  loadct,0  
  tvscl,s5

  ; show the linear-corrected dB image in a smaller window for review purposes
  s6=rebin(t3,300,600)
  window,10,xsize=300,ysize=600,title="Corrected in Linear"
  loadct,0
  tvscl,s6


  ; Which method works better? Explain why one of them works better.
  ; Explain why you obtain clearly different results in cases (a) and (b).
  ; 
  ; [assignment note] Along with a small different in mean intensity, the visual appearance of both images is quite similar.
  ; At the smaller dimensions of the windows above (chosen for ease of display on my desktop) the difference is hard to distinguish.
  ; When displayed at the original dimensions, 3000x6000, then I can perhaps distinguish a slight difference between t2 and t3.
  ; 
  ; Visual details in the dB-corrected image t2 more closely correlate with t0 -- e.g. minor black speckles in t0 match up in t2 but not in t3.
  ; One image area with land features (from 1000:1000) shows more significant differences -- t2 appears visually more like t0 here;
  ; t2 intensity here appears nearly the same as t0 (even allowing that t0 still has the scallop artifact), yet t3 appears much 'darker' than both.
  ; 
  ; Overall, I think the visual appearance of the dB-corrected image t2 works better than the linear-corrected image t3.
  ; Although, the visual appearance is not the key problem with t3
  ; As can be see in the corrected mean intensity profile plot for t3 above -- that value equals 1.0 which is very suspicious for t3.
  ;
  ; What I think has occurred here is that the outcome of dividing each pixel of t3 by the mean of this swath has affected the mean variably:
  ; for those pixels within that same swath against which the mean was divided, statistically that results in an average quotient of 1.0
  ; And since the right-side of the image, where the mean was determined, is more intense on average: for other parts of the image the
  ; artifact-reduction calculation will statistically result in a reduction of intensity by dividing against this more intense mean.
  ; Hence, overall many areas of t3 present a 'darker' appearance and the mean(t3) value is lower.
  ; 
  ; Whatever choice of swath for the mean calculation is used, conducting a division operation to remove this mean (and the artifact)
  ; will always produce a skewness of intensity across t3, and some swath in the image will have that same 1.0 mean value.
  ; It seems that some other function than simple division should be used to remove the mean, if that is possible.
  ; Even better is to just conduct the operation and artifact removal in the decibel domain as was done initially.
  ; 
  ; To elaborate further on this topic, the manually inserted artifact was a good simulation of the 'scalloping' which occurs in ScanSAR images.
  ; Such scalloping is best observed in areas of an image that are uniform, such as the ocean, and where motion also occurs (e.g. a forest).
  ; With ScanSAR platform, residual scalloping is caused by a mismatch of inaccurate Doppler centroid frequency (as discussed by Dr. Romeiser).
  ; For this actual scalloping, one way to remove that is to subtract a dB image containing ONLY the scalloping effect
  ; (as an inverse pattern, or more accurately a weighted integral of segments of the pattern), as best as possible
  ; from the original dB image and thereby produce as a difference an image in which most of the scalloping has been removed.
  ; 
  ; In this problem, instead of subtracting a simulated image with only the scalloping effect, we are subtracting the mean intensity --
  ; and because the amplitude of that mean intensity is greatly skewed by the scalloping effect, the concept is somewhat similar to the
  ; real approach mentioned above -- but that approach works best in the dB domain and shows that in the linear domain the concept is flawed.


  ; When you think you have found a good solution for the correction procedure and selected the better of the two results,
  ; I have a little surprise for you: Open a window of size 600 × 1200

  window,11,xsize=600,ysize=1200
  ; and type the following, assuming that d1 is your artifact-corrected linear image (not dB image):
  tvscl,congrid(d1,600,1200)/rebin(d1,600,1200)

  ; You should see something very different than the SAR image. It may look a little noisy, but you should be able to recognize something.
  ;
  ; [assignment note] Congratulations to you and your bearded student! (perhaps at his PhD award ceremony?)
  ;
  ; Can you explain what I have done to make this work?
  ;
  ; [assignment note] I suggest that the 2nd image was merged into the SAR image and appears in the difference between congrid/rebin functions.
  ; The rebinned image alone does not reveal the 2nd image (also not shown when I had displayed it in smaller window earlier).
  ; From the IDL manual: the REBIN function simply resizes a vector or array to dimensions given by the parameters.
  ; Here, the supplied dimensions (600 x 1200) are integral factors of the original dimension.
  ;
  ; Likewise, the congrid image alone does not reveal the 2nd image either.
  ; From the IDL manual: the CONGRID function shrinks the size of the array by an arbitrary amount.
  ; Unlike REBIN, CONGRID will resize an array to any arbitrary size (not just an integer multiple of the original size)
  ;
  ;
  ; (Try doing additional tests, analyzing the range of values of different arrays, etc.)
  ;
  ; The difference between congrid/rebin is apparent when considering other factors for the resizing operations.
  ; For instance, these factors did not result in the ghost image appearing:
  ; IDL> tvscl,congrid(d1,750,1500)/rebin(d1,750,1500)   = factor = 4
  ; IDL> tvscl,congrid(d1,500,1000)/rebin(d1,500,1000)   = factor = 6
  ; IDL> tvscl,congrid(d1,375,750)/rebin(d1,375,750)     = factor = 8
  ;
  ; While these factors did result in the ghost image appearing:
  ; IDL> tvscl,congrid(d1,600,1200)/rebin(d1,600,1200)   = factor = 5 (original)
  ; quite faintly:
  ; IDL> tvscl,congrid(d1,300,600)/rebin(d1,300,600)     = factor = 10
  ; and even more faintly,
  ; IDL> tvscl,congrid(d1,150,300)/rebin(d1,150,300)     = factor = 20
  ;
  ;
  ; Provide an explanation for the surprise effect that demonstrates a basic understanding of it.
  ;
  ; I think the 2nd ghost image of the two fellows was inserted into the original SAR image as follows:
  ; starting with the original format of the SAR image, the 2nd ghost had been rebinned to match these dimensions (3000x6000), and then
  ; at every 5th horizontal line of both images, pixels on that line from the ghost image were multiplied by some factor with pixels in the
  ; original image, producing a combined product every 5th line. That's why resizing at factors of 5 reveals the ghost image, while resizing
  ; at other factors does not reveal it. Also, determining the factor of multiplication -- and thus the intensity of the inserted ghost image --
  ; may be possible, and some speculation is attempted here next.
  ;
  ; We also know that since we are using the tvscl function, there is some display scaling occurring.
  ; From the IDL manual: The TVSCL procedure scales the intensity values of Image into the range of the image display device
  ; and outputs the data to the image display at the specified location. The array is scaled so that the minimum data value
  ; becomes 0 and the maximum value becomes the maximum number of available colors.
  ;
  ; If we use the tv function instead, as follows, 
  ; IDL> tv,congrid(d1,600,1200)/rebin(d1,600,1200)
  ; then the output appears completely black; the ghost image is not visible until multiplying this output by a factor of at least 25.
  ; By experimentation, it was determined that the command,
  ; IDL> tv,congrid(d1,600,1200)/rebin(d1,600,1200)*865
  ; produces an identical output to the previous tvscl commend.
  ; 
  ; While interesting, I don't think the value 865 can be used to directly determine the ghost image's intensity. Although the tvscl function
  ; multiplies the input values, as mentioned, by the maximum number of available colors (probably 256 in grayscale), even given this we
  ; don't seem to have enough information to determine that intensity from the remaining terms: (data - datamin) / (datamax - datamin)
  ;
  ; Continuing on to further observations, we can see that:
  ; mean(d1) = 0.44447148
  ; mean(congrid(d1,600,1200)) = 0.45260420
  ; mean(rebin(d1,600,1200)) = 0.45117390
  ; mean(congrid(d1,600,1200))/mean(rebin(d1,600,1200)) = 1.0031701
  ; mean(congrid(d1,600,1200))/mean(rebin(d1,600,1200,/sample)) = 1.0000
  ; 
  ; As described later, when rebin is used with the sample keyword, then the output is the same as congrid: more information is contained therein.
  ; So the mean difference, 1.0031701-1.0000 = 0.0031701, approximately 1/315, ought to be near to the factor by which the ghost image was mixed in.
  ;
  ; The explanation for the effect is also hinted at by the operation of the rebin function and its minifying options,
  ; for instance, when executing the command differently, as follows,
  ; IDL> tvscl,congrid(d1,600,1200)/rebin(d1,600,1200,SAMPLE=0)
  ; then the 2nd ghost image of the two fellows remains as before.
  ; 
  ; Yet, when executing the command as,
  ; IDL> tvscl,congrid(d1,600,1200)/rebin(d1,600,1200,/sample)
  ; or as follows,
  ; IDL> tvscl,congrid(d1,600,1200)/rebin(d1,600,1200,SAMPLE=1)
  ; then the 2nd ghost image of the two fellows vanishes, as mentioned above.
  ; 
  ; This occurs because there is a difference when rebin is used with sampling keyword (/sample, or /sample=1) or without (/sample=0 or null).
  ; From the IDL manual: Normally, REBIN uses bilinear interpolation when magnifying and neighborhood AVERAGING when minifying.
  ; So, in this case of minifying our image and using the default output of rebin, this uses neighborhood AVERAGING.
  ;
  ; From the IDL manual: Set the SAMPLE keyword to use nearest neighbor SAMPLING for both magnification and minification.
  ; Thus, it can be seen that when using SAMPLING, then rebin uses the nearest neighbor SAMPLING method instead.
  ; 
  ; In comparison to rebin, congrid uses bilinear interpolation by default and congrid will always resample the array.
  ; So, when using congrid on our artifact-corrected linear image, to shrink that to 600x1200, it preserves the 2nd ghost image that was mixed in.
  ; Then when using rebin with neighborhood SAMPLING, to also shrink that to 600x1200, it also preserves the 2nd ghost image that was mixed in.
  ; Since the 2nd image is preserved in both minified images, the quotient when dividing between these outputs is null: there is no ghost image.
  ; 
  ; However, when instead using rebin with neighborhood AVERAGING (the default option), the 2nd ghost image is not preserved by rebin.
  ; As rebin averages over neighboring pixels, the ghost pixels in every 5th horizontal line are effectively "averaged out" of the minified image.
  ; Thus, a difference between the congrid-shrunk and that rebin-shrunk image reveals the ghost image that was preserved in the congrid-shrunk image.
  ; 
  ; These functions of rebin and congrid can be observed by noting that the following command,
  ; IDL> tvscl,rebin(d1,600,1200,SAMPLE=1)/rebin(d1,600,1200)
  ; produces the same ghost image output as the suggested command, showing how congrid and rebin with sampling both function practically the same.
  ;
  ; As an aside, another interesting observation is that the following commands produced an INVERSE ghost image,
  ; IDL> tvscl,rebin(d1,600,1200)/congrid(d1,600,1200)
  ; IDL> tvscl,rebin(d1,600,1200)/rebin(d1,600,1200,SAMPLE=1)
  ;
  ;
  ; Finally, to comment further on congrid - this function has options to use bilinear interpolation (default), linear or cubic interpolation.
  ; Further tests were tried using the CUBIC and INTERP keywords for congrid, comparing their difference with the default congrid.
  ; For example, the following commands did show differences between the different types of interpolation,
  ; IDL> tvscl,congrid(d1,400,800)/congrid(d1,400,800,/interp)
  ; IDL> tvscl,congrid(d1,400,800)/congrid(d1,400,800,cubic=-0.015)
  ; producing a ghost output representing their difference (mainly apparent in the land areas at the lower left).
  ;
  ; However, when the multiple of the resize was constrained to an integer factor of the original dimensions, such as,
  ; IDL> tvscl,congrid(d1,300,600)/congrid(d1,300,600,/interp)
  ; Then no difference was observed between the linear, bilinear, and cubic methods of congrid.
  ; At an integer factor of the original dimensions, all three interpolation methods of congrid seem to produce the same result, with no difference.
  ; Thus, these different interpolation methods of congrid could not be used to reveal the 2nd ghost image in their difference,
  ; because the 2nd ghost image was inserted at dimensions 600x1200, which was an integer factor of the original dimensions.
  ;
  ;
  ; [assignment note] A final approach to the problem I considered was to multiply the original image with the inverse of simulated scalloping.
  ; This method (as described for removing scalloping in TopSAR images) implies the additional step of simulating scalloping characteristics.
  ; I had thought to try this by estimating a set of sine waves that would produce a similar scalloping pattern and intensity
  ; as seen in the mean intensity profile, and then roughly producing a corresponding set of sine functions.
  ; But this proved to be rather inefficient without knowing more about how the original scalloping was produced
  ; (whether manually, in the case of this artifact, or by an antenna pattern in the case of an actual TopSAR image).
  ; And my initial results were at all useful as compared to just using the mean profile, so this method was not attempted further.

  stop

end