; front signature extraction demo after midterm exam
; based on midterm_test roro 18 mar 2016
; roro 20 mar 2020

pro midterm_demo

; load image
t0=fltarr(1500,3000)
openr,1,'C:\roland\oce687\2020\midterm\front.flt'
readu,1,t0
close,1

; have a look
window,0,xsize=500,ysize=1000
loadct,0
tvscl,rebin(t0,500,1000)<10000.0
stop   ; enter .cont to continue

; we will work with lines 500 to 1299 (800 lines)

; have another look to validate that this is a good range
t0(*, 499)=max(t0)
t0(*,1300)=max(t0)
tvscl,rebin(t0,500,1000)<10000.0
stop   ; enter .cont to continue

; reduce image to area of interest, so we can display it in full size
t1=t0(*,500:1299)
window,1,xsize=1500,ysize=800
tvscl,t1<10000.0
stop   ; enter .cont to continue

; first attempt to find the shape of the front:
; just look for maximum in each line
ip=intarr(800)
for j=0,799 do begin
    tj=t1(*,j)
    ip(j)=(where(tj eq max(tj)))(0)
endfor
window,4,xsize=1500,ysize=800
; in the following line, indgen(800) creates an array of 800 integer values from 0 to 799
; plotting ip,indgen(800) makes ip the x variable and indgen(800) the y variable
plot,ip,indgen(800)
stop   ; enter .cont to continue

; this first profile is noisy because many lines have brightest pixel outside front

; try to smooth image before looking for maxima, so isolated spikes will be reduced
t2=t1
for j=0,2 do t2=smooth(t2,3,/edge_truncate)
for j=0,799 do begin
    tj=t2(*,j)
    ip(j)=(where(tj eq max(tj)))(0)
endfor
loadct,33
oplot,ip,indgen(800),color=224,thick=2
stop   ; enter .cont to continue

; this is not bad except for top part

; let's use just the lower 700 lines - that's still a lot of lines
ip=ip(0:699)
loadct,0
; in the following line, xrange=[0,1500] forces the x range to 0 to 1500, like in the noisy case
plot,ip,indgen(750),xrange=[0,1500]
stop   ; enter .cont to continue

; now show the derived front shape in the image itself
; as black line, 3 pixels wide
t2=t1
for j=0,699 do t2(ip(j)-1:ip(j)+1,j)=0.0
wset,1
tvscl,t2<10000.0
wshow,1
stop   ; enter .cont to continue

; now compute the mean intensity profile, as a 101-pixel wide profile around the front
p1=fltarr(101)
for j=0,699 do p1(*)=p1(*)+t1(ip(j)-50:ip(j)+50,j)
p1=p1/700.0
wset,4
; when plotting the profile, use median as reference level
plot,p1/median(p1)
wshow,4
wshow,0

stop

end